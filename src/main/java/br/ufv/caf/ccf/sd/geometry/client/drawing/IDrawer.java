package br.ufv.caf.ccf.sd.geometry.client.drawing;

import br.ufv.caf.ccf.sd.geometry.common.model.Geometry;

import java.awt.*;

public interface IDrawer {
    void draw(Graphics g);

    void addGeometry(Geometry g);

}
