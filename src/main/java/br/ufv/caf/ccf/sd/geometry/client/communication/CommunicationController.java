package br.ufv.caf.ccf.sd.geometry.client.communication;

import br.ufv.caf.ccf.sd.geometry.client.drawing.IDrawer;

public class CommunicationController {

    private IPCClient current;

    private IDrawer drawer;

    public CommunicationController(IPCClient method, IDrawer drawer) {
        changeIPCMethod(method);
        this.drawer = drawer;
    }

    public void generateNewGeometry() {
        drawer.addGeometry(current.requestGeometry());
    }

    public void changeIPCMethod(IPCClient method) {
        current = method;
    }

}
