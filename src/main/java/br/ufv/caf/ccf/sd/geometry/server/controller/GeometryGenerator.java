package br.ufv.caf.ccf.sd.geometry.server.controller;

import br.ufv.caf.ccf.sd.geometry.common.model.Geometry;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface GeometryGenerator extends Remote {
    Geometry generateGeometry() throws RemoteException;
}