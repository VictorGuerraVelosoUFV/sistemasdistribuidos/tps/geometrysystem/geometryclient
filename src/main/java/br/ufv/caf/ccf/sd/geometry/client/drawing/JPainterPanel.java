package br.ufv.caf.ccf.sd.geometry.client.drawing;

import javax.swing.*;
import java.awt.*;

public class JPainterPanel extends JPanel {
    private IDrawer drawer;

    public IDrawer getDrawer() {
        return drawer;
    }

    public JPainterPanel() {
        super();
        this.drawer = new DrawingController();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        drawer.draw(g);
    }
}
