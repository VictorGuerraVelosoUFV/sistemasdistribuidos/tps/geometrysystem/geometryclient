package br.ufv.caf.ccf.sd.geometry.client.communication;

import br.ufv.caf.ccf.sd.geometry.common.model.Geometry;
import gherkin.deps.com.google.gson.Gson;

import java.io.*;
import java.net.Socket;

public class SocketAdapter implements IPCClient {
    private final Gson g;

    public SocketAdapter() {
        g = new Gson();
    }

    @Override
    public Geometry requestGeometry() {

        String sentence;
        Socket clientSocket;
        try {
            clientSocket = new Socket("127.0.0.1", 8080);
            DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
            DataInputStream inFromServer = new DataInputStream(clientSocket.getInputStream());
            outToServer.writeBytes("GeometryGenerate");
            outToServer.flush();
            sentence = inFromServer.readLine();
            if (sentence.startsWith("Error wrong command")){
                throw new IOException("Message corrupted");
            }
            System.out.println("FROM SERVER: " + sentence);
            clientSocket.close();
            return g.fromJson(sentence, Geometry.class);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
