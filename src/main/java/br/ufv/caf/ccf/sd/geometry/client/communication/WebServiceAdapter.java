package br.ufv.caf.ccf.sd.geometry.client.communication;

import br.ufv.caf.ccf.sd.geometry.common.model.Geometry;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import gherkin.deps.com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class WebServiceAdapter implements IPCClient {
    private HttpClient client;
    private HttpGet request;
    private final Gson g;

    public WebServiceAdapter() {
        client = new DefaultHttpClient();
        request = new HttpGet("http://127.0.0.1:5000/");
        g = new Gson();
    }

    @Override
    public Geometry requestGeometry() {
        try {
            HttpResponse response = client.execute(request);
            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(
                            response.getEntity().getContent()));
            StringBuilder result = new StringBuilder();
            String line;
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            System.out.println(result.toString());
            return g.fromJson(result.toString(), Geometry.class);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }
}
