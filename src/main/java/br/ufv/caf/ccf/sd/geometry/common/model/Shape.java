package br.ufv.caf.ccf.sd.geometry.common.model;

import java.io.Serializable;

public enum Shape implements Serializable {
    TRIANGLE, SQUARE, CIRCLE, RECTANGLE
}
