package br.ufv.caf.ccf.sd.geometry.client.communication;

import br.ufv.caf.ccf.sd.geometry.common.model.Geometry;

public interface IPCClient {
    Geometry requestGeometry();
}
