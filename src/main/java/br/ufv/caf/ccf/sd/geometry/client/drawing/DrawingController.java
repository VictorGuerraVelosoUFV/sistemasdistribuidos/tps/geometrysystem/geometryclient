package br.ufv.caf.ccf.sd.geometry.client.drawing;

import br.ufv.caf.ccf.sd.geometry.common.model.Geometry;
import br.ufv.caf.ccf.sd.geometry.common.model.Position;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collection;

public class DrawingController implements IDrawer {

    private Collection<Geometry> geometry;

    public DrawingController() {
        geometry = new ArrayList<>(0);
    }

    @Override
    public void draw(Graphics g) {
        for (Geometry geo : geometry) {
            switch (geo.getShape()) {
                case TRIANGLE:
                    drawTriangle(geo, g);
                    break;
                case SQUARE:
                case RECTANGLE:
                    g.drawRect(geo.getPos().getX(), geo.getPos().getY(), geo.getWidth(), geo.getHeight());
                    break;
                case CIRCLE:
                    g.drawOval(geo.getPos().getX(), geo.getPos().getY(), geo.getWidth(), geo.getHeight());
                    break;
            }
        }
    }

    private void drawTriangle(Geometry triangle, Graphics g) {
        Position p1 = new Position(triangle.getPos().getX() + (triangle.getWidth() / 2), triangle.getPos().getY());
        Position p2 = new Position(triangle.getPos().getX(), triangle.getPos().getY() + triangle.getHeight());
        Position p3 = new Position(triangle.getPos().getX() + triangle.getWidth(), triangle.getPos().getY() + triangle.getHeight());
        int[] px = {p1.getX(), p2.getX(), p3.getX()};
        int[] py = {p1.getY(), p2.getY(), p3.getY()};
        g.drawPolygon(px, py, 3);
    }

    @Override
    public void addGeometry(Geometry g) {
        if (g != null) {
            geometry.add(g);
        }
    }
}
