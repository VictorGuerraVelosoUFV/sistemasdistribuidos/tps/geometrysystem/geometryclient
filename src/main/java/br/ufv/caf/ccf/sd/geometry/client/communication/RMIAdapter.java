package br.ufv.caf.ccf.sd.geometry.client.communication;

import br.ufv.caf.ccf.sd.geometry.server.controller.GeometryGenerator;
import br.ufv.caf.ccf.sd.geometry.common.model.Geometry;

import java.io.File;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class RMIAdapter implements IPCClient {

    private GeometryGenerator gen;

    public RMIAdapter() {
        System.setProperty("java.security.policy", "file://".concat(new File("").getAbsolutePath().concat("/src/all.policy")));
        System.setSecurityManager(new SecurityManager());
        try {
            gen = (GeometryGenerator) Naming.lookup("rmi://127.0.0.1:1098/GeometryGenerator");
        } catch (RemoteException | NotBoundException | MalformedURLException e) {
            e.printStackTrace();
            gen = null;
        }
    }

    @Override
    public Geometry requestGeometry() {
        try {
            return gen.generateGeometry();
        } catch (RemoteException e) {
            e.printStackTrace();
            return null;
        }
    }
}