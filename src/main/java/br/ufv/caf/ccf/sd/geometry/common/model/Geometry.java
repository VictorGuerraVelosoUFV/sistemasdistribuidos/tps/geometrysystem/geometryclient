package br.ufv.caf.ccf.sd.geometry.common.model;

import java.io.Serializable;

public class Geometry implements Serializable {

    private int width;

    private int height;

    public Shape getShape() {
        return shape;
    }

    private Position pos;

    private final Shape shape;

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public Position getPos() {
        return pos;
    }

    public Geometry() {
        this(Shape.TRIANGLE);
    }

    public Geometry(Shape s) {
        this(s, 0, 0, new Position(0, 0));
    }

    public Geometry(Shape s, int w, int h, Position p) {
        width = w;
        height = h;
        shape = s;
        pos = p;
    }
}
