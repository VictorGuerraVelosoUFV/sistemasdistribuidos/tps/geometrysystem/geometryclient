package br.ufv.caf.ccf.sd.geometry.client.communication;

import br.ufv.caf.ccf.sd.geometry.client.drawing.JPainterPanel;

import javax.swing.*;
import java.awt.*;

public class MainView {
    private static IPCClient rmi;
    private static SocketAdapter socket;
    private static WebServiceAdapter webservice;
    private JPanel mainPanel;
    private JButton pedirButton;
    private JPanel painterPanel;
    private JComboBox<IPCClient> serviceSelector;
    private CommunicationController communicationController;

    public static void main(String[] args) {
        JFrame janela = new JFrame("Janela");
        janela.setMinimumSize(new Dimension(768, 768));
        MainView mv = new MainView();
        janela.setContentPane(mv.mainPanel);
        janela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        janela.pack();
        janela.setVisible(true);
    }

    private MainView() {
        pedirButton.addActionListener(actionEvent -> {communicationController.generateNewGeometry(); painterPanel.updateUI();});
        socket = new SocketAdapter();
        serviceSelector.addItem(socket);
        rmi = new RMIAdapter();
        serviceSelector.addItem(rmi);
        webservice = new WebServiceAdapter();
        serviceSelector.addItem(webservice);
        serviceSelector.setRenderer(new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                setText(((IPCClient)value).getClass().getSimpleName());
                return this;
            }
        } );
        serviceSelector.addItemListener(itemEvent -> communicationController.changeIPCMethod((IPCClient) itemEvent.getItem()));
        communicationController = new CommunicationController(serviceSelector.getItemAt(serviceSelector.getSelectedIndex()),
                                                              ((JPainterPanel)painterPanel).getDrawer());
    }

    private void createUIComponents() {
        painterPanel = new JPainterPanel();
    }
}
